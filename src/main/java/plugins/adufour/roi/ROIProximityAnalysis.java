package plugins.adufour.roi;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.DoubleStream;

import org.apache.poi.ss.usermodel.Workbook;

import icy.canvas.IcyCanvas;
import icy.canvas.IcyCanvas2D;
import icy.gui.frame.progress.AnnounceFrame;
import icy.painter.Overlay;
import icy.plugin.abstract_.Plugin;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.sequence.SequenceDataIterator;
import icy.type.point.Point3D;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarColor;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarEnum;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.adufour.vars.lang.VarSequence;
import plugins.adufour.vars.lang.VarWorkbook;
import plugins.adufour.workbooks.IcySpreadSheet;
import plugins.adufour.workbooks.Workbooks;
import plugins.kernel.roi.descriptor.measure.ROIMassCenterDescriptorsPlugin;

public class ROIProximityAnalysis extends Plugin implements Block
{
    public enum ProximityDistanceType
    {
        CENTER_TO_CENTER;

        @Override
        public String toString()
        {
            return super.toString().toLowerCase().replaceAll("_", " ");
        }
    }

    public enum ProximityEventType
    {
        CLOSEST_DISTANCE, ALL_VALID_DISTANCES;

        @Override
        public String toString()
        {
            return super.toString().toLowerCase().replaceAll("_", " ");
        }
    }

    VarSequence varSequence = new VarSequence("Sequence", null);

    VarROIArray varRoiA = new VarROIArray("ROI - group A");

    VarROIArray varRoiB = new VarROIArray("ROI - group B");

    VarDouble varMaxDistance = new VarDouble("Max. distance (\u00B5m)", 0.5);

    VarEnum<ProximityDistanceType> varProxDistType = new VarEnum<ProximityDistanceType>("Measure",
            ProximityDistanceType.CENTER_TO_CENTER);

    VarEnum<ProximityEventType> varProxEventType = new VarEnum<ProximityEventType>("Keep",
            ProximityEventType.CLOSEST_DISTANCE);

    VarColor varLayerColor = new VarColor("Display color", Color.yellow);

    VarWorkbook varWorkbook = new VarWorkbook("Workbook", (Workbook) null);

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("sequence", varSequence);
        inputMap.add("roiA", varRoiA);
        inputMap.add("roiB", varRoiB);
        inputMap.add("measure", varProxDistType);
        inputMap.add("distance", varMaxDistance);
        inputMap.add("proxType", varProxEventType);
        inputMap.add("color", varLayerColor);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("workbook", varWorkbook);
    }

    @Override
    public void run()
    {
        if (varProxDistType.getValue() != ProximityDistanceType.CENTER_TO_CENTER)
            throw new UnsupportedOperationException(varProxDistType.getName());

        Sequence seq = varSequence.getValue(true);

        // Remove any existing Proximity overlay
        for (Overlay overlay : seq.getOverlays(ProximityOverlay.class))
            seq.removeOverlay(overlay);

        ProximityOverlay overlay = new ProximityOverlay(varLayerColor.getValue());
        seq.addOverlay(overlay);

        String seqName = seq.getName();
        double pxSizeX = seq.getPixelSizeX();
        double pxSizeY = seq.getPixelSizeY();
        double pxSizeZ = seq.getPixelSizeZ();

        int nChannels = seq.getSizeC();

        ROI[] roiA = varRoiA.getValue(true);
        ROI[] roiB = varRoiB.getValue(true);

        int nbProximityEvents = 0;
        List<Double> allDistances = new ArrayList<Double>();

        Workbook wb = Workbooks.createEmptyWorkbook();

        IcySpreadSheet summary = Workbooks.getSheet(wb, "Proximity (summary)");
        summary.setRow(0, "Dataset", "Nb. events", "Min. dist. (\u00B5m)", "Avg. dist. (\u00B5m)",
                "Max. dist. (\u00B5m)");
        IcySpreadSheet details = Workbooks.getSheet(wb, "Proximity (details)");
        details.setRow(0, "Dataset", "ROI A", "ROI B", "Dist. (\u00B5m)");

        for (int c = 0; c < nChannels; c++)
        {
            details.setValue(0, 4 + c, "A: Avg " + seq.getChannelName(c));
            details.setValue(0, 4 + nChannels + c, "B: Avg " + seq.getChannelName(c));
        }

        try
        {
            for (int iA = 0; iA < roiA.length; iA++)
            {
                ROI a = roiA[iA];
                Point3D centerA;
                centerA = ROIMassCenterDescriptorsPlugin.computeMassCenter(a).toPoint3D();

                double[] avgA = avgIntensities(a, seq);

                // In case only the closest distance is required, prepare to store the pair
                double closestDistance_AB = Double.MAX_VALUE;
                ROI closestB = null;
                Point3D closestCenterB = null;

                // in case roiA and roiB refer to the same list, don't count things twice
                for (int iB = (roiA == roiB) ? iA + 1 : 0; iB < roiB.length; iB++)
                {
                    ROI b = roiB[iB];

                    double distance_AB = Double.NaN;

                    // Measure the distance between A and B in metric units
                    Point3D centerB = ROIMassCenterDescriptorsPlugin.computeMassCenter(b).toPoint3D();
                    distance_AB = Point3D.getDistance(centerA, centerB, pxSizeX, pxSizeY, pxSizeZ); // @Stephane:

                    if (distance_AB < varMaxDistance.getValue())
                    {
                        // Potential event
                        if (varProxEventType.getValue() == ProximityEventType.CLOSEST_DISTANCE
                                && distance_AB < closestDistance_AB)
                        {
                            // Remember this
                            closestDistance_AB = distance_AB;
                            closestB = b;
                            closestCenterB = centerB;
                        }
                        else
                        {
                            // Valid event no matter what
                            allDistances.add(distance_AB);
                            nbProximityEvents++;
                            details.setRow(nbProximityEvents, seqName, a.getName(), b.getName(), distance_AB);
                            overlay.addPair(centerA, centerB);

                            double[] avgB = avgIntensities(b, seq);
                            for (int c = 0; c < nChannels; c++)
                            {
                                details.setValue(nbProximityEvents, 4 + c, avgA[c]);
                                details.setValue(nbProximityEvents, 4 + nChannels + c, avgB[c]);
                            }
                        }
                    }
                }

                if (varProxEventType.getValue() == ProximityEventType.CLOSEST_DISTANCE && closestB != null)
                {
                    // Only one valid event
                    allDistances.add(closestDistance_AB);
                    nbProximityEvents++;
                    details.setRow(nbProximityEvents, seqName, a.getName(), closestB.getName(), closestDistance_AB);
                    overlay.addPair(centerA, closestCenterB);

                    double[] avgB = avgIntensities(closestB, seq);
                    for (int c = 0; c < nChannels; c++)
                    {
                        details.setValue(nbProximityEvents, 4 + c, avgA[c]);
                        details.setValue(nbProximityEvents, 4 + nChannels + c, avgB[c]);
                    }
                }
            }
        }
        catch (InterruptedException e)
        {
            // ignore

        }

        // Write a summary as well
        DoubleStream values = allDistances.stream().mapToDouble(Double::doubleValue);
        DoubleSummaryStatistics stats = values.summaryStatistics();
        summary.setRow(1, seqName, stats.getCount(), stats.getMin(), stats.getAverage(), stats.getMax());

        varWorkbook.setValue(wb);
    }

    private static double[] avgIntensities(ROI roi, Sequence seq)
    {
        int nChannels = seq.getSizeC();

        double[] avg = new double[nChannels];

        for (int c = 0; c < nChannels; c++)
        {
            SequenceDataIterator iterator = new SequenceDataIterator(seq, roi, false, -1, -1, c);

            // minimum / maximum / sum
            double sum = 0, cpt = 0;
            while (!iterator.done())
            {
                sum += iterator.get();
                cpt++;
                iterator.next();
            }
            avg[c] = sum / cpt;
        }

        return avg;
    }

    private static class ProximityOverlay extends Overlay
    {
        private final Color color;

        private Map<Point3D, List<Point3D>> links = new HashMap<Point3D, List<Point3D>>();

        private AnnounceFrame errorFrame = null;

        public ProximityOverlay(Color color)
        {
            super("Proximity Analysis");
            this.color = color;
        }

        public void addPair(Point3D a, Point3D b)
        {
            if (!links.containsKey(a))
                links.put(a, new ArrayList<Point3D>());

            links.get(a).add(b);
            painterChanged();
        }

        @Override
        public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
        {
            // just handle the usual 2D canvas
            if (canvas instanceof IcyCanvas2D)
            {
                Graphics2D g2 = (Graphics2D) g.create();
                g2.setColor(color);
                g2.setStroke(new BasicStroke((float) canvas.canvasToImageLogDeltaX(2)));

                Line2D.Double line = new Line2D.Double();
                for (Point3D a : links.keySet())
                {
                    line.x1 = a.getX();
                    line.y1 = a.getY();
                    for (Point3D b : links.get(a))
                    {
                        line.x2 = b.getX();
                        line.y2 = b.getY();
                        g2.draw(line);
                    }
                }
            }
            else if (errorFrame == null)
            {
                errorFrame = new AnnounceFrame(
                        "NB: the proximity layer only works in 2D views (yet).\nFor more options please submit your request on the Icy forum.");
            }
        }
    }
}
